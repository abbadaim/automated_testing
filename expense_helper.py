from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
import time


class ExpenseHelper(object):
    # static variables
    finish_loading = ec.invisibility_of_element_located(
        (By.CSS_SELECTOR, 'input.select2-input.select2-focused.select2-active'))

    searched_item = 'div.select2-result-label:nth-child(1)'

    def __init__(self, driver):
        self.driver = driver

    def new_expense(self):
        self.driver.find_element_by_css_selector('a[href="/expenses/new"]').click()
        print('creating expense')

    def is_using_mc(self):
        try:
            self.driver.find_element_by_id('s2id_transaction_currency_list_id')
            return True
        except NoSuchElementException:
            return False

    def select_payfrom(self, account):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_refund_from_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select2_payfrom = 's2id_autogen17_search'
            else:
                select2_payfrom = 's2id_autogen16_search'
            self.driver.find_element_by_id(select2_payfrom).send_keys(account)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def tick_pay_later(self):
        self.driver.find_element_by_id('transaction_expense_payable').click()

    def select_beneficiary(self, beneficiary):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_person_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select2_beneficiary = 's2id_autogen12_search'
            else:
                select2_beneficiary = 's2id_autogen11_search'
            self.driver.find_element_by_id(select2_beneficiary).send_keys(beneficiary)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def set_transaction_date(self, date):
        date_field = self.driver.find_element_by_id('transaction_transaction_date')
        date_field.click()
        date_field.clear()
        date_field.send_keys(date)
        self.driver.find_element_by_css_selector('td.active.day').click()

    def set_payment_method(self, method):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_payment_method_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select2_pay_method = 's2id_autogen2_search'
            else:
                select2_pay_method = 's2id_autogen2_search'
            self.driver.find_element_by_id(select2_pay_method).send_keys(method)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def add_tags(self, tags):
        self.is_using_mc()
        for tag in tags:
            self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
            if self.is_using_mc() is True:
                select2_tag = 's2id_autogen11'
            else:
                select2_tag = 's2id_autogen10'
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(tag)
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(Keys.RETURN)
            self.driver.find_element_by_css_selector('label.control-label').click()

    def add_first_account(self, account, tax, amount):
        self.is_using_mc()

        # add first account
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_account_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                first_account = 's2id_autogen18_search'
            else:
                first_account = 's2id_autogen17_search'
            self.driver.find_element_by_id(first_account).send_keys(account)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            selected_account = self.driver.find_element_by_css_selector(self.searched_item)
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(selected_account)
            action.click(selected_account)
            action.perform()

        # add first tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_line_tax_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                first_tax = 's2id_autogen13_search'
            else:
                first_tax = 's2id_autogen12_search'
            self.driver.find_element_by_id(first_tax).send_keys(tax)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set first amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_0_debit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def add_second_account(self, account, tax, amount):
        self.is_using_mc()

        # add secong account
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_1_account_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                second_account = 's2id_autogen19_search'
            else:
                second_account = 's2id_autogen18_search'
            self.driver.find_element_by_id(second_account).send_keys(account)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            selected_account = self.driver.find_element_by_css_selector(self.searched_item)
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(selected_account)
            action.click(selected_account)
            action.perform()

        # add first tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_1_line_tax_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                second_tax = 's2id_autogen14_search'
            else:
                second_tax = 's2id_autogen13_search'
            self.driver.find_element_by_id(second_tax).send_keys(tax)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set first amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_1_debit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def use_mc(self, currency, rate=''):
        self.driver.find_element_by_id('s2id_transaction_currency_list_id').click()
        input_mc = self.driver.find_element_by_id('s2id_autogen3_search')
        input_mc.click()
        input_mc.send_keys(currency)
        self.driver.find_element_by_css_selector('span.select2-match').click()
        if rate != '':
            time.sleep(1)
            self.driver.find_element_by_id('edit_currency_href').click()
            WebDriverWait(self.driver, 10).until(ec.presence_of_element_located((By.CSS_SELECTOR, 'div.modal-dialog')))
            custom_currency = self.driver.find_element_by_id('custom_currency_field')
            custom_currency.click()
            custom_currency.send_keys(rate)
            self.driver.find_element_by_css_selector('button.btn.btn-success.edit_multi_currency').click()
        else:
            pass

    def insert_witholding_amount(self, amount, unit, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('witholding_toggle').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if unit != '%':
            self.driver.find_element_by_id('btn-toggle-value').click()
        else:
            pass
        witholding_value = self.driver.find_element_by_name('transaction[witholding_value]')
        witholding_value.clear()
        witholding_value.send_keys(amount)
        # self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        self.driver.find_element_by_id('s2id_witholding_account').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select_witholding_account = 's2id_autogen20_search'
            else:
                select_witholding_account = 's2id_autogen19_search'
            self.driver.find_element_by_id(select_witholding_account).send_keys(account_number)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def create_expense(self):
        self.driver.find_element_by_id('create_button').click()

    def add_memo(self, memo):
        addmemo = self.driver.find_element_by_id('transaction_memo')
        addmemo.click()
        addmemo.send_keys(memo)

