from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from select2helper import Select2Helper

class BankDepositHelper(object):

	def __init__(self, driver):
	    self.driver = driver
	    self.sh = Select2Helper(driver)
	    self.wait = WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])

	def create_new_bankdeposit(self):
		try:
			self.wait.until(
				ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.overlay-background'))
				)
		finally:
			self.driver.find_element_by_class_name('btn-action-recon').click()
			self.driver.find_element_by_xpath('//*[@id="main-content"]/section/div[2]/div/div/div[1]/div[2]/div/div/ul/li[2]').click()

	def select_depositto(self, depositto):
		self.driver.find_element_by_id('s2id_transaction_deposit_to_id').click()
		self.driver.find_element_by_id('s2id_autogen14_search').send_keys(depositto)
		self.sh.select2()

	def select_payer(self, payer):
		self.driver.find_element_by_id('s2id_transaction_person_id').click()
		self.driver.find_element_by_id('s2id_autogen8_search').send_keys(payer)
		self.sh.select2()

	def set_transaction_date(self, date):
	    date_field = self.driver.find_element_by_id('transaction_transaction_date')
	    date_field.click()
	    date_field.clear()
	    date_field.send_keys(date)

	def select_tags(self, tag):
		action = ActionChains(self.driver)
		self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
		tag_field = self.driver.find_element_by_id('s2id_autogen7')
		tag_field.clear()
		tag_field.send_keys(tag)
		try:
			self.wait.until(
	        	ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
	        	)
		finally:
			element = self.driver.find_element_by_xpath('//*[@id="select2-drop"]/ul/li')
			action.move_to_element(element)
			action.click()
			action.perform()

	def tax_inclusive_toggle(self):
		self.driver.find_element_by_class_name('tax-inclusive-toogle').click()	

	def scroll_page(self):
		element = self.driver.find_element_by_id('create_button')
		actions = ActionChains(self.driver)
		actions.move_to_element(element).perform()	

	def select_account(self, account):
		self.driver.find_element_by_class_name('transaction_product').click()
		self.driver.find_element_by_id('s2id_autogen15_search').send_keys(account)
		self.sh.select2()

	def add_description(self, description):
		self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_0_description').send_keys(description)

	def select_tax(self, selecttax):
		self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_line_tax_id').click()
		self.driver.find_element_by_id('s2id_autogen9_search').send_keys(selecttax)
		try:
			self.wait.until(
				ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
				)
		except NoSuchElementException:
			self.driver.find_element_by_class_name('select2-drop-mask').click()
			print('tax not found')
		finally:
			self.driver.find_element_by_class_name('select2-result-label').click()

	def add_tax(self, newtax, taxamount):
		self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_line_tax_id').click()
		self.driver.find_element_by_css_selector('add_new_product_link_child:nth-child(1)').click()
		try:
			self.wait.until(
				ec.element_to_be_clickable((By.ID, 'company_tax_name'))
				)
		finally:
			taxname = self.driver.find_element_by_id('company_tax_name')
			taxname.click()
			taxname.send_keys(newtax)
			taxrate = self.driver.find_element_by_id('company_tax_rate')
			taxrate.click()
			taxrate.send_keys(taxamount)
			self.driver.find_element_by_id('add-new-company-tax').click()	
		
	def add_amount(self, amount):
		addamount = self.driver.find_element_by_xpath('//*[@id="transaction_transaction_account_lines_attributes_0_credit"]')
		addamount.click()
		addamount.send_keys(amount)

	def add_memo(self, memo):
		addmemo = self.driver.find_element_by_xpath('//*[@id="transaction_memo"]')
		addmemo.click()
		addmemo.send_keys(memo)

	def click_create_button(self):
		self.driver.find_element_by_xpath('//*[@id="create_button"]').click()
	