from selenium import webdriver
from jurnal_navigations import Navigations
from salesorder_helper import SalesOrderHelper
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException
from selenium.common.exceptions import WebDriverException

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
so = SalesOrderHelper(driver)

n.open_newblueranger()
n.login_aim()
# n.activate_automation_company()
n.open_sales_index()

so.create_new_salesorder()
so.select_customer('Costumer 1')
so.tick_shipping()
so.transaction_address('jl. auto auto no. 110')
so.tick_shipping_same_as_address()
so.ship_via('jne')
so.tracking_number('123456789')
so.set_transaction_date('01/01/2018')
so.select_tags('tag1')
so.tax_inclusive_toggle()
so.scroll_page()
so.select_product('produk 1')
so.add_qty('10')
so.add_price('10000')
so.add_discount('10')
so.select_tax('PPN')
so.click_anywhere()
so.add_message('automated testing')
so.add_memo('automated')
so.click_create_button()
so.check_if_created()

n.close_browser()